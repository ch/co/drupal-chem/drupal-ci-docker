FROM ubuntu:20.04
RUN ln -fs /usr/share/zoneinfo/Europe/London /etc/localtime
RUN DEBIAN_FRONTEND=noninteractive apt-get update && apt-get install -y \
  mariadb-server \
  php \
  apache2 \
  libapache2-mod-php \
  php-mysql \
  php-xml \
  php-mbstring \
  php-gd \
  php-ldap \
  wget \
  git  \
  unzip \
  && rm -rf /var/lib/apt/lists

ADD chemistry.gpg /etc/apt/trusted.gpg.d
RUN echo "deb https://downloads.ch.cam.ac.uk/local-debs $(cat /etc/lsb-release | grep DISTRIB_CODENAME | cut -d '=' -f 2) ucamchem " > /etc/apt/sources.list.d/chemistry.list

RUN DEBIAN_FRONTEND=noninteractive apt-get update && apt-get install -y \
  drupal-site-management \
  && rm -rf /var/lib/apt/lists

RUN wget https://github.com/drush-ops/drush/releases/download/8.4.5/drush.phar -O /usr/local/bin/drush && chmod 755 /usr/local/bin/drush

RUN git clone https://gitlab.developers.cam.ac.uk/ch/co/drupal-chem/makesite /tmp/makesite
# makesite.sh requires that these exist but because we later call that with "-s" they don't get used
RUN touch /tmp/makesite/external_users.sql /tmp/makesite/external_cache_symplectic.sql
# makesite.sh writes an include_once() directive for these files. On real servers they're managed by ansible;
# here we just need to make sure the include_once() does not fail
RUN mkdir -p /etc/drupal-chem && touch /etc/drupal-chem/chemistry_db.php
RUN mkdir -p /etc/drupal-chem && touch /etc/drupal-chem/elements.php

RUN /bin/bash -c "/usr/bin/mysqld_safe  &" && \
  sleep 5 && \
  mkdir /var/www/drupal && \
  /tmp/makesite/makesite.sh -i localhost -d "Test site" -s

COPY start-service.sh /root/
RUN chmod +x /root/start-service.sh
CMD ["/bin/bash"]
